package testApp;

import java.util.concurrent.TimeUnit;
/**
 * Once per minute one item will be displayed
 * @author 
 *
 */
public class Listing implements Runnable {
	private final static double ZERO_AMOUNT = 0.0;
	private final static double EPSILON = 0.000_000_000_000_000_001;

	@Override
	public void run() {
		System.out.println("-----------Payments----------");
		for (Payment item : PaymentUtil.getPayments()) {
			// zero amount will not be displayed
			if (Math.abs(item.getMoney() - ZERO_AMOUNT) > EPSILON) {
				System.out.println(item);
			}
		}
		System.out.println("-----------------------------");
	}
}