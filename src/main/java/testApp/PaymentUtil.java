package testApp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.IllegalFormatException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import testApp.ReaderFactory.SourceType;

public class PaymentUtil {
	private static final String INPUT_STRING_PATTERN = "[A-Z]{3}\\s\\-?\\+?\\d+\\.?\\d+";
	private static final String QUIT = "QUIT";
	private static List<Payment> payments = Collections.synchronizedList(new LinkedList<Payment>());

	/**
	 * Getter
	 * 
	 * @return list of payments
	 */
	public static List<Payment> getPayments() {
		return payments;
	}

	/**
	 * Loading and creating the payment list
	 * 
	 * @param type
	 *            source type (CONSOLE|TXT_FILE)
	 * @param source
	 *            data source, for TXT type path, for CONSOLE null
	 * @return list of payments
	 */
	public static void readData(final ReaderFactory.SourceType type, final String source, List<Payment> paymentList) {
		String currentLine = null;
		try (BufferedReader reader = ReaderFactory.getReaderInstance(source, type)) {
			String[] result = null;
			double money = 0.0;
			String currency = null;
			while ((currentLine = reader.readLine()) != null) {
				if (validateData(currentLine)) {
					Payment tempPayment = parseData(currentLine);
					addPayment(tempPayment, paymentList);
				} else if (currentLine.equalsIgnoreCase(QUIT)) {
					System.exit(0);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method creates a payment from input data
	 * 
	 * @param data
	 *            currency and money
	 * @return new Payment
	 */
	public static Payment parseData(final String data) {
		final String result[] = data.split("\\s");
		final String currency = result[0];
		final double money = Double.parseDouble(result[1]);

		return new Payment(currency, money);
	}

	/**
	 * Purpose of this method is add payment to the list of payments. There will
	 * be only one currency for each payment no more
	 * 
	 * @param payment
	 *            payment
	 * @param paymentList
	 *            list of payments
	 */
	public static void addPayment(Payment payment, List<Payment> paymentList) {
		if (paymentList.contains(payment)) {
			for (Payment item : paymentList) {
				if (item.equals(payment)) {
					item.setMoney(item.getMoney() + payment.getMoney());
					break;
				}
			}
		} else {
			paymentList.add(payment);
		}
	}

	/**
	 * Method for validation input strings
	 * 
	 * @param data
	 *            input validated string
	 * @return TRUE if data is valid, FALSE if not
	 */
	public static boolean validateData(final String data) {
		Pattern pattern = Pattern.compile(INPUT_STRING_PATTERN);
		Matcher matcher = pattern.matcher(data);
		return matcher.matches();
	}
}
