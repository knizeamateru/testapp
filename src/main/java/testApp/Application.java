package testApp;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application {

	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

	public static void main(String... args) {
		Payment.setExchangeRates("CZK", 2.);
		Payment.setExchangeRates("HKD", 3.);
		Payment.setExchangeRates("EUR", 5.);

		if (args.length == 1) {
			PaymentUtil.readData(ReaderFactory.SourceType.TXT_FILE, args[0], PaymentUtil.getPayments());
		}

		scheduler.execute(new Input());
		// Once per minute one item will be displayed
		scheduler.scheduleWithFixedDelay(new Listing(), 0, 60, TimeUnit.SECONDS);
	}

	/**
	 * Getter
	 * @return 
	 */
	public static ScheduledExecutorService getScheduler() {
		return scheduler;
	}
}
