package testApp;

import java.util.Scanner;

import testApp.ReaderFactory.SourceType;

/**
 * This implementation is for user inputs (shutting down and payments)
 * 
 * @author
 *
 */
public class Input implements Runnable {
	private final static String QUIT = "quit";
	private static Scanner sc = new Scanner(System.in);

	@Override
	public void run() {
		while (Boolean.TRUE) {
			String newLine = sc.nextLine();
			if (newLine.equalsIgnoreCase(QUIT)) {
				Application.getScheduler().shutdownNow();
				System.exit(0);
			} else {
				if(PaymentUtil.validateData(newLine)){
					Payment tempPayment = PaymentUtil.parseData(newLine);
					PaymentUtil.addPayment(tempPayment, PaymentUtil.getPayments());
				}
			}
		}
	}
}
