package testApp;

import java.util.AbstractMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Representation of payment (currency and money)
 * 
 * @author
 *
 */
public class Payment {

	private static AbstractMap<String, Double> exchangeRates = new ConcurrentHashMap<String, Double>();
	private final static String BASE_CURRENCY = "USD";
	private final String currency;
	private double money;
	private final static double DEFAULT_RATE = 1.;

	/**
	 * Constructor
	 * 
	 * @param currency
	 *            currency name
	 * @param money
	 *            amount
	 */
	public Payment(final String currency, final double money) {
		this.currency = currency;
		this.money = money;
	}

	/**
	 * Setter
	 * 
	 * @param currency
	 *            currency name
	 * @param rate
	 *            rate value
	 */
	public static void setExchangeRates(final String currency, final Double rate) {
		exchangeRates.put(currency, rate);
	}

	/**
	 * Getter
	 * 
	 * @param currency
	 *            currency name
	 * @return rate value if exist, default rate if not
	 */
	public static double getExchangeRates(final String currency) {
		if (exchangeRates.containsKey(currency)) {
			return exchangeRates.get(currency);
		} else {
			return DEFAULT_RATE;
		}
	}

	/**
	 * Setter
	 * 
	 * @param money
	 */
	public void setMoney(final double money) {
		this.money = money;
	}

	/**
	 * Getter
	 * 
	 * @return
	 */
	public double getMoney() {
		return this.money;
	}

	@Override
	public String toString() {
		if (this.currency.equalsIgnoreCase(BASE_CURRENCY)) {
			return this.currency + " " + this.money;
		} else {
			return this.currency + " " + this.money + " " + BASE_CURRENCY + " ("
					+ (this.money / getExchangeRates(this.currency)) + ")";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		return true;
	}
}
