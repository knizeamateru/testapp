package testApp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;

public class ReaderFactory {
	public enum SourceType {
		CONSOLE, TXT_FILE
	};

	public static BufferedReader getReaderInstance(final String path, final ReaderFactory.SourceType type) {
		BufferedReader reader = null;
		switch (type) {
		case TXT_FILE:
			try {
				reader = new BufferedReader(new FileReader(path));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			break;
		default:
			reader = new BufferedReader(new InputStreamReader(System.in));
		}
		return reader;

	}
}
